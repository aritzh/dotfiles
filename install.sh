#!/usr/bin/env bash

cd "$(dirname "$0")"

shopt -s nullglob

nPWD=$PWD
nPWD="~${nPWD#$HOME}"

printf "Backing up existing files and directories to $nPWD/.old\n"

mkdir -p "$PWD/.old"
for f in `ls -pA1 --color=never | grep -v / | grep -v "directory" | grep -v ".git[^_]" | grep -o "^\..*$"`
do
        if [ -f "$HOME/$f" ]; then
                cp "$HOME/$f" "$PWD/.old/" &&
                        printf "  %-17s -> %s\n" "~/$f" "$nPWD/.old/$f" ||
                        printf "  Error backing up %s" "~/$f"
        fi
done
if [ -f "$HOME/.extra-env-vars" ]; then
        cp "$HOME/.extra-env-vars" "$PWD/.old" &&
                        printf "  %-17s -> %s\n" "~/.extra_env_vars" "$nPWD/.old/.extra_env_vars" ||
                        printf "  Error backing up %s!" "~/$f"
fi

for f in `ls --color=never -Ap1 | grep -P "^\..*/" | grep -v "git" | grep -o "^.*[^/]"`
do
        if [ -d "$HOME/$f" ]; then
                cp -r "$HOME/$f" "$PWD/.old/" &&
                        printf "  %-17s -> %s\n" "~/$f/" "$nPWD/.old/$f/" ||
                        printf "  Error backing up %s!" "~/$f/"
        fi
done

if [ -f "$PWD/.oh-my-zsh/oh-my-zsh.sh" ]; then
        printf "Git submodules already initialized\n"
else
        git submodule update --init --recursive # In case it wasn't a recursive clone
        printf "Initialized git submodules\n"
fi

printf "Creating symlinks for dot-files\n"
for f in `ls -pA1 --color=never | grep -v / | grep -v "directory" | grep -v ".git[^_]" | grep -o "^\..*$"`
do
	rm -f "$HOME/$f"
	2>/dev/null 1>/dev/null ln -s "$PWD/$f" "$HOME/$f" &&
		printf "  %-17s -> %s\n" "~/$f" "$nPWD/$f" ||
		printf "  Error linking to file %s!" "~/$f"
done

printf "Creating symlinks for dot-directories\n"

for f in `ls --color=never -Ap1 | grep -P "^\..*/" | grep -v "git" | grep -o "^.*[^/]"`
do
	rm -f "$HOME/$f"
	2>/dev/null 1>/dev/null ln -s "$PWD/$f" "$HOME/$f" &&
		printf "  %-17s -> %s\n" "~/$f/" "$nPWD/$f/" ||
		printf "  Error linking to folder %s!" "~/$f/"
done

printf "Registering environment variables\n"

rm -f "$HOME/.extra_env_vars"
# Export the current dir as $DOT_FILES_DIR
echo "export DOT_FILES_DIR=$PWD" >  "$HOME/.extra_env_vars"

