" Lines that have a vertical bar at the end is to allow the end of line comment,
" Because vim by default does not have them, so the vertical bar breaks the line
" as if they where two lines.
set nocompatible|     " Use Vim settings, rather than Vi settings. Must be first
set encoding=utf-8|   " Set UTF-8 encoding
scriptencoding utf-8

filetype off|                      " Required by Vundle
set rtp+=~/.vim/bundle/Vundle.vim| " Add Vundle to the runtime path and initialize
call vundle#begin()|               " All plugins should go after this

Plugin 'VundleVim/Vundle.vim'|             " Vundle auto-handles itself
"Plugin 'jdonaldson/vaxe'|                  " Haxe plugin
" Plugin 'altercation/vim-colors-solarized'| " Solarized color scheme
Plugin 'scrooloose/nerdtree'|              " Better tree directory explorer
Plugin 'Xuyuanp/nerdtree-git-plugin'|      " Nerdtree extension to integrate with git
Plugin 'bling/vim-airline'|                " Best status line plugin
Plugin 'bronson/vim-trailing-whitespace'|  " Highlights trailing whitespace, and adds :FixWhitespace to remove it

call vundle#end()|         " All Plugins should go before this
filetype plugin indent on| " Required by vundle

" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal


if has('mouse')| " Enable mouse if possible.
  set mouse=a
endif

autocmd FileType text setlocal textwidth=100| " For all text files set 'textwidth' to 100 characters.

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
autocmd BufReadPost *
\ if line("'\"") >= 1 && line("'\"") <= line("$") |
\   exe "normal! g`\"" |
\ endif

au BufWritePre * let &bex = '-' . strftime("%Y%m%d-%H%M%S") . '.vimbackup'| " Set backup file

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
        \ | wincmd p | diffthis
endif

syntax on

colorscheme solarized8

set background=dark|                                     " Use dark solarized
set autowrite|                                           " Save file when switching to another buffer
set backspace=indent,eol,start|                          " Allow backspace everywhere on insert mode
set backup|                                              " keep a backup file (restore to previous version)
set undofile|                                            " Keep an undo file (undo changes after closing)
set backupdir=~/.vim/tmp//|                              " Set backup directory
set directory=~/.vim/tmp//|                              " Set swap files directory
set undodir=~/.vim/tmp//|                                " Set undos directory
set esckeys|                                             " Should speed up going to normal mode
set expandtab|                                           " Use spaces instead of tabs
set guifont=Ubuntu\ Mono\ derivative\ Powerline|         " Select the font to be used. Should be Powerline compatible.
set history=50|                                          " Keep 50 lines of command line history
set incsearch|                                           " Do incremental searching (search as you type)
set laststatus=2|                                        " Always show status line
set listchars=eol:$,tab:│-,trail:~,extends:>,precedes:<| " When using :set list, set how to highlight
set noerrorbells|                                        " Do not beep on error
set visualbell|                                          " Flash the screen on error
set nowrap|                                              " Long lines are not visually broken down into several (i.e one file line, one terminal line).
set number|                                              " Show line numbers
set ruler|                                               " Show the cursor position all the time
set showcmd|                                             " Display incomplete commands

" Make backup directory if does not exist
let a = system('mkdir ~/.vim/tmp')

let &t_Co=256| " Set terminal colors

" Airline specific settings
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_powerline_fonts = 1
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_mode_map = {
    \ '__' : '-',
    \ 'n'  : 'N',
    \ 'i'  : 'I',
    \ 'R'  : 'R',
    \ 'c'  : 'C',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
\ }

map Q <Nop>|                          " Don't use Ex mode (ugly-old vi thingy)
map <C-n> :NERDTreeToggle<CR>|        " Ctrl+n -> Open NerdTree
map <F5> <Esc>:silent make\|redraw!\|cc<CR>|     " F5 -> Make/Compile
" Allow saving of files as sudo when I forgot to start vim using sudo.
cnoremap w!! execute 'write !sudo tee % >/dev/null' <bar> edit!

nnoremap <C-k4>    :tabp<CR>|         " Ctrl+Numpad4 -> Previous tab
nnoremap <C-k6>    :tabn<CR>|         " Ctrl+Numpad6 -> Previous tab
nnoremap <C-t>     :tabnew<CR>|       " Ctrl+t -> New tab
nnoremap <C-w>     :tabclose<CR>|     " Ctrl+w -> Close current tab

inoremap <C-k4>     <Esc>:tabp<CR>i|  " Ctrl+Numpad4 -> Previous tab
inoremap <C-k6>     <Esc>:tabn<C>i|   " Ctrl+Numpad6 -> Previous tab
inoremap <C-t>      <Esc>:tabnew<CR>| " Ctrl+t -> New tab
inoremap <C-w>      :tabclose<CR>|    " Ctrl+w -> Close current tab

inoremap <C-u> <C-g>u<C-u>|           " Make Ctrl+u undoable
inoremap <c-w> <c-g>u<c-w>|           " Make Ctrl+w undoable

filetype on
autocmd BufRead,BufNewFile *.nxc set filetype=NotExactlyC
autocmd BufRead,BufNewFile *.nxc set syntax=c
autocmd Filetype NotExactlyC setlocal makeprg=nbcc\ %
autocmd Filetype NotExactlyC setlocal efm=%f:::%l:::%m
autocmd QuickFixCmdPost [^l]* cwindow 4
autocmd QuickFixCmdPost    l* lwindow
aug QFClose
          au!
            au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&buftype") == "quickfix"|q|endif
    aug END
