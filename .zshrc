
# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# Path to the custom zsh theme and plugins
export ZSH_CUSTOM="$HOME/dotfiles/oh-my-zsh-custom"

# Theme configuration
ZSH_THEME="spaceship"
SPACESHIP_PROMPT_ADD_NEWLINE="false"
SPACESHIP_PROMPT_SEPARATE_LINE="false"
SPACESHIP_TIME_SHOW="true"
SPACESHIP_EXIT_CODE_SHOW="true"
SPACESHIP_DIR_PREFIX=""
SPACESHIP_EXEC_TIME_ELAPSED="30"
SPACESHIP_EXIT_CODE_SYMBOL=""
SPACESHIP_DIR_TRUNC_REPO="false"
SPACESHIP_DIR_TRUNC="0"
SPACESHIP_USER_SHOW="false"
SPACESHIP_CHAR_SYMBOL="$(if [ $UID -eq 0 ]; then echo "#"; else echo "➜"; fi)"
SPACESHIP_CHAR_SUFFIX=" "

HYPHEN_INSENSITIVE="true"
HIST_STAMPS="dd/mm/yyyy"

plugins=(
  git-extras
  zsh-syntax-highlighting
  zsh-autosuggestions
  colored-man-pages
  autojump
  common-aliases
  pip
)

source $ZSH/oh-my-zsh.sh

export EDITOR='vim'

